﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class CardInfo:IGetDictionary
    {

        private int _expiry = 0;

        /// <summary>
        /// 身份证
        /// </summary>
        public string card { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string name { get; set; }


        /// <summary>
        /// 出生日期
        /// </summary>
        public string birthDate { get; set; }


        /// <summary>
        /// 制卡日期
        /// </summary>
        public string makingCardDate { get; set; }


        /// <summary>
        /// 证件有效期
        /// </summary>
        public string expiry 
        {
            get => _expiry == 0 ? "永久有效" : _expiry.ToString();
            set => _expiry = Int32.Parse(value);
        }

        
        /// <summary>
        /// 地址
        /// </summary>
        public string address { get; set; }


        /// <summary>
        /// 民族
        /// </summary>
        public string nation { get; set; }



      

        public string GetInfoStr
        {
            get
            {
                var str = new StringBuilder();
                str.Append(card);
                str.Append("\r\n");

                str.Append(name);
                str.Append("\r\n");


                str.Append(nation);
                str.Append("\r\n");


                str.Append(makingCardDate);
                str.Append("\r\n");


                str.Append(expiry);
                str.Append("\r\n");


                str.Append(address);
                str.Append("\r\n");


                return str.ToString();

            }
        }


        ICollection<KeyValuePair<string, string>> IGetDictionary.GetDic() 
        {

            return new Dictionary<string, string>()
            {
                {"card",this.card },
                {"name",this.name },
                {"expiry",this._expiry.ToString() },
                {"nation",this.nation },
                {"makingCardDate",this.makingCardDate },
                {"birthDate",this.birthDate },
                {"address",this.address }
            };

        }
    }
}
