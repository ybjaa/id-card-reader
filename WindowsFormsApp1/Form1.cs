﻿using eloamComLib;
using System;
using System.Text;
using System.Windows.Forms;
using System.Media;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {



        private EloamGlobal global;

        private string imagePath = @"d:\card_image";


        public Form1()
        {
            InitializeComponent();
            global = new EloamGlobal();

            //二代证
            global.IdCard += IdCardEventHandler;


            global.InitDevs();
            global.InitIdCard();
            global.DiscernIdCard();

            global.InitReader();
            global.ReaderStart();
        }

       

        public async void initCSRF()
        {
            var result=await httpMyBackEnd.RefreshCSRF();
            textBox3.Text = result.message;
        }
      



        //二代证事件响应
        public async void IdCardEventHandler(int ret)
        {

            string name = global.GetIdCardData(1);
            string card = global.GetIdCardData(8);
            string birthDate = $"{global.GetIdCardData(4)}-{global.GetIdCardData(5)}-{global.GetIdCardData(6)}";
            string makingCardDate= $"{global.GetIdCardData(10)}-{global.GetIdCardData(11)}-{global.GetIdCardData(12)}";
            string expiryStr = global.GetIdCardData(13);
            string address = global.GetIdCardData(7);
            string nation = global.GetIdCardData(3);

            int expiry = 0;
            Int32.TryParse(expiryStr, out expiry);

            if (expiry != 0)
            {
                expiry = expiry - Int32.Parse(global.GetIdCardData(10));
            }

            var cardInfo = new CardInfo();

            cardInfo.card = card;
            cardInfo.makingCardDate = makingCardDate;
            cardInfo.name = name;
            cardInfo.birthDate = birthDate;
            cardInfo.expiry = expiry.ToString();
            cardInfo.address = address;
            cardInfo.nation = nation;


            textBox2.Text = cardInfo.GetInfoStr;




            EloamImage img = (EloamImage)global.GetIdCardImage(1);

            img.Resize(358, 441, 0);
            // img.Whiten(0, -1, 0.85f, 5, 180);
           

            img.Save($"{imagePath}\\{card}_{name}.jpg", 0x0080);
            img.Destroy();

            img = null;


            SoundPlayer player = new SoundPlayer(".\\13893.wav");
            player.Play();


            var result=await httpMyBackEnd.PostJson("IDCard/AddCardInfo", cardInfo);

            if (!result.result)
            {
                addText3($"数据传输出错:{result.message}");
            }

            

        }



        private void addText3(string text)
        {
            textBox3.AppendText("\r\n\r\n" + text);
            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            global.IdCard -= IdCardEventHandler; 

            global.DeinitIdCard();
            global.StopIdCardDiscern();
            global.DeinitDevs();

            global.ReaderStop();
            global.DeinitReader();

            global = null;
           
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            

            DialogResult dialog = folderBrowserDialog1.ShowDialog();

            if (dialog == DialogResult.OK)
            {
                imagePath = folderBrowserDialog1.SelectedPath;
                textBox1.Text = imagePath;
            }
            

        }




        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Text = imagePath;
            folderBrowserDialog1.SelectedPath = imagePath;
            initCSRF();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox3.Text = "";
            
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            var result = await httpMyBackEnd.RefreshCSRF();
            addText3(result.message);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
            //System.IO.Stream s = a.GetManifestResourceStream(@"13890.wav");
            
        }
    }
}
