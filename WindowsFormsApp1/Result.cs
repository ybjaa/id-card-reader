﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Result : IResult
    {


        public bool result { get ; set ; }
        public string message { get ; set ; }


        public Result(bool result=false,string message="")
        {
            this.result = result;
            this.message = message;
        }
    }
}
