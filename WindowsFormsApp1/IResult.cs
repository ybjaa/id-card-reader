﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    interface IResult
    {
        bool result { get; set; }

        string message { get; set; }

    }
}
